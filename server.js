const express = require('express');
const path = require('path')
const cookieParser = require('cookie-parser');
const bcrypt = require('bcrypt');
const { PORT = 3000 } = process.env;
const { Sequelize, QueryTypes } = require('sequelize');
require('dotenv').config();

// Initialize the App.
const app = express();

// Setup Middleware applications.
app.use(express.json());
app.use(cookieParser());
// Expose JavaScript, Images and CSS files under the alias public/
app.use('/public', express.static(path.join(__dirname, 'www')));

// initialize the database connection
const sequelize = new Sequelize({
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE,
    dialect: process.env.DB_DIALECT
});





function ignoreFavicon(req, res, next) {
    if (req.originalUrl === '/favicon.ico') {
      res.status(204).json({nope: true});
    } else {
      next();
    }
  }



// Check if we can establish a connection to the database.
sequelize.authenticate().then(d => {
    // app.use(ignoreFavicon);
    // Successfully connected to the Database.
    app.use(async (req, res, next) => { //  NOTE: This is an async function -> You can use await.

        // 1. Allow / to pass through 
        // If user tries to load /dashboard it should redirect to / -- See slides.
        
        const publicPaths = ['/', '/api/login', '/api/register'];
        
        if (publicPaths.includes( req.path)){

            
            return next();
        }

        // 2. Check if the req cookie -> token exists.

        console.log(req.cookies);
        
           
                if(!req.cookies.token){
                    console.log('No Token cookie');
                    return res.redirect('/')
                }

           

                // 3. get the token from the request cookie 
                   const {token} = req.cookies;


                   try {

                    const resultToken = await sequelize.query('SELECT * FROM usersession WHERE token = :token AND active =1', {
                        type: QueryTypes.SELECT,
                        replacements: {
                            token: token
                        }
                     });

                     
                     

                     if (resultToken.length ===0){

                        return res.status(403).send('You are not authorized to view this page, <br> <a href="/">Go back to login</a>');

                     }






                       
                   } catch (e) {
                       
                   }
                   
                    

                                       
                        
                       
                         
                     
                    
                     
                    
                    







    

        // 4. Check the database for the token and if it is active = 1

        // 5. Check if the user linked to the token exists

        // 6. Return a status 401 If it is expired or the user does not Exists


        // Should be the last line of code here.
        return next();
    });

    


    app.get('/', (req, res) => { // this is our index page (or login)
        return res.status(200).sendFile(path.join(__dirname, 'www', 'index.html'))
    
    });

    app.get('/dashboard', (req, res) => { // Must be a protected page. Can't access without token cookie.
        return res.status(200).sendFile(path.join(__dirname, 'www', 'dashboard.html'));
 
    });

    

    
 app.post('/api/register', async(req, res)=>{
    const username = 'test3';
    const password = 'kossa';
    try{
        const hashPassword = await bcrypt.hash(password, 10);
        const result = await sequelize.query('INSERT INTO user (username, password) VALUE (:username, :password)', {
            type: QueryTypes.INSERT,
            replacements: {
                username: username,
                password: hashPassword
            }
        })
    } catch (e) {
        return res.json(e);
    }
    return res.json('Done...');
});




    app.post('/api/login', async (req, res) => { // Authentication Request.

        // Check the login
        // 1. Get the username and password from the req.body
        const { username, password } = req.body;

        if(!username || !password){
            return res.status(400).json({
                message: 'Please provide a username and password'
            });
        }

       
        try {

            // 2. Check if the username exists (Select the user based on the username)
            console.log('Number 1');
            
            let user = await sequelize.query('SELECT * FROM user WHERE username = :username', {
                type: QueryTypes.SELECT,
                replacements: {
                    username: username
                }
             });
             console.log('Number 3');
             console.log(user);
             console.log(user.length);
             
             
             
            if (user.length === 0){

                return res.status(401).json({
                    message:'Invalid user. That user might not exist...'
                });
            
            }
            
            
        // 3. Check if the password in the db matches the password given.
        // NOTE: use bcrypt (Already required at the top) - To hash the password with 10 rounds.
        // For help see: https://www.npmjs.com/package/bcrypt 
        // Check the section on Promises:
        
        
        // Load hash from your password DB. Sample using Async/Await:
        const pwResult = await bcrypt.compare(password, user[0].password);
        // -> true if it works. 
        console.log(pwResult);

               // 4. If true Generate a token using the HASH_SECRET from the .env file (it will be on the process.env)
               if (!pwResult){
                   console.log('false here');
                   
                return res.status(401).json({
                    message:'Invalid password'
                });
             }
                    
               
                const token = await bcrypt.hash(process.env.HASH_SECRET, 10);
    
                
                const result = await sequelize.query('INSERT INTO usersession (userId, token) VALUE (:userId, :token)', {
                    type: QueryTypes.INSERT,
                    replacements: {
                        userId: user[0].userId,
                         token: token
                    }
                })
                  // 5. NB - Send the token back to the client.
                   
                return res.status(201).json({
                    status: 201,
                    token: token
                });
                
    
            
    
            // 6. End.
            
            
    
            
            
        }
        catch (e) {
            return res.status(500).json({
                message: e.tostring()
            });
            
        }




        

 
    });


    app.listen(PORT, () => console.log(`Server has started on port ${PORT}...`));


}).catch(e => {
    console.error(e);
})



