function App() {
    this.elUsername = document.getElementById('username');
    this.elPassword = document.getElementById('password');
    this.elBtnLogin = document.getElementById('btn-login');
}

class CookieManager {
    
    static setCookie(name, value) {
        let d = (new Date(Date.now() + 900000));
        document.cookie = `${name}=${value}; expires=${d}`; 
    }
    static getCookie(name) {

        let cookieArr = document.cookie.split('; ');
        let nameValuePair = [];
          for (let i = 0; i < cookieArr.length; i++) {
            nameValuePair = cookieArr[i].split('=');
            if ( name === nameValuePair[0] ) {
                return nameValuePair[1];
            }
        }
        return false;
    }
    static clearCookie(name) {
        document.cookie = `${name}=; max-age=0`; 
    }
}








App.prototype.init = function () {

    // check for token in cookie
    const tokenCookie = CookieManager.getCookie('token');

    // verify cookie
    // not sure how to verify 

    // redirect to Dashboard

    this.elBtnLogin.addEventListener('click', async e => {
        // Prevent the form from Submitting.
        e.preventDefault();

       
        if(!this.elPassword.value|| !this.elUsername.value ){
            return alert('Must enter a Username and Password. It cannot be blank')
            
        }


        try {
                      
            const result = await fetch('http://localhost:3000/api/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: this.elUsername.value.trim(),
                    password: this.elPassword.value.trim()
                })
                
            })
            .then(r => r.json());

            if (result.status >= 400){
                
                 alert(result.message);
                
            }
            
            

            // 1. Store the token in a cookie called token.
                document.cookie = 'token=' +result.token;
                // 2. Redirect to Dashboard.
                window.location.href = '/dashboard';
            



        } catch (e) {
            alert(e.toString());
        }


    });
    
}

new App().init();